﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;

namespace client
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:5000");


            ContentNegociation_Json(client);
            ContentNegociation_Xml(client);
            
            RawHttpRequest();

        }


#region sample code

        public static void ContentNegociation_Json(HttpClient client) 
        {
            Console.WriteLine("Start JSON request");
            Console.WriteLine("--------------------------------------------------------------");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "values");
            request.Method = HttpMethod.Get;
            request.Headers.TryAddWithoutValidation("Accept", "application/json");
            Console.WriteLine("HTTP Request: "+ request.ToString());
            var response = client.SendAsync(request).Result;
            Console.WriteLine("HTTP Response: " + response.ToString());
            Console.WriteLine("Content: " + response.Content.ReadAsStringAsync().Result);

            Console.WriteLine("End JSON request");
            Console.WriteLine("--------------------------------------------------------------");
        }

        public static void ContentNegociation_Xml(HttpClient client) 
        {
            Console.WriteLine("Start XML request");
            Console.WriteLine("--------------------------------------------------------------");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "values");
            request.Method = HttpMethod.Get;
            request.Headers.TryAddWithoutValidation("Accept", "text/xml");
            Console.WriteLine("HTTP Request: "+ request.ToString());
            var response = client.SendAsync(request).Result;
            Console.WriteLine("HTTP Response: " + response.ToString());
            Console.WriteLine("Content: " + response.Content.ReadAsStringAsync().Result);

            Console.WriteLine("End JSON request");
            Console.WriteLine("--------------------------------------------------------------");
        }

        public static void RawHttpRequest()
        {
            //this can be replaced with any non-TLS endpoint
            //the request to google.ro wll return a BadRequest response
            var host = "www.google.ro";
            var resource = "/";

            var hostEntry = Dns.GetHostEntry(host);
            var socket = CreateSocket(hostEntry);
            var requestMessage = string.Format(
                "GET {0} HTTP/2 \r\n" + 
                "Host {1} \r\n" +
                "\r\n", resource, host
            );

            var requestBytes = Encoding.ASCII.GetBytes(requestMessage);
            socket.Send(requestBytes);
            var response = GetSocketResponse(socket);
            Console.WriteLine(response);
        }

        private static object GetSocketResponse(Socket socket)
        {
            var bytes = 0;
            byte[] buffer = new byte[512];
            var result = new StringBuilder();
            do{
                bytes = socket.Receive(buffer);
                result.Append(Encoding.ASCII.GetString(buffer, 0, bytes));
            }while(bytes > 0);

            return result.ToString();
        }

        private static Socket CreateSocket(IPHostEntry hostEntry)
        {
            int httpPort = 80;
            var endPoint = new IPEndPoint(hostEntry.AddressList[0], httpPort);
            var socket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);
            if (socket.Connected)
            {
                return socket;
            }
            return null;
        }
#endregion
        
        
        //Documentation: https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client


        public static void CreateValue(HttpClient client) {
            //implement create here
        }

        public static void ReadValues(HttpClient client)
        {
            //read all values here
        }

        public static void ReadValue(HttpClient client, string id) {
            //read a particular value here
        }

        public static void UpdateValue(HttpClient client, string id, string newValue) {
            //TODO
        }

        public static void DeleteValue(HttpClient client, string id) {
            //TODO
        }

    }
}
