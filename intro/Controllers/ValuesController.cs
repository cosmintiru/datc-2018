using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace intro.Controllers
{
    public class ValuesController : Controller
    {
        private static IDictionary<string, object> Resource = new Dictionary<string, object>();

        [HttpGet("values")]
        public IActionResult Read() 
        {
            return Ok(new string[]{"123", "321"});
        }

        [HttpGet("values/{id}")]
        public IActionResult Read(string id){
            return Ok(Resource[id]);
        }

        [HttpPost("values")]
        public IActionResult Create([FromBody] string value) {
            var id = Guid.NewGuid().ToString();
            Resource.Add(id, value);
            return Created(new Uri("http://localhost:5000/values/"+id), value);
        }
        [HttpPut("values/{id}")]
        public IActionResult Update(string id, [FromBody] string value) {
            if(Resource.ContainsKey(id))
                Resource.Remove(id);
            Resource.Add(id, value);
            return Ok();
        }

        [HttpDelete("values")]
        public IActionResult Delete(string id){
            Resource.Remove(id);
            return Ok();
        }
    }
}