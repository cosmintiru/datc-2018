### Environment

All the samples that will be uploaded here use Visual Studio Code/dotnet core 2.1 which can be downloaded from here:

1. https://code.visualstudio.com/
2. https://www.microsoft.com/net/download

No detailed instruction will be described here since all the platform details are already in the links. You can either use Windows, Linux or Macs.

## Run the web app

```bash
cd /git-clone-folder/intro
code .
dotnet build
dotnet run
```

This will expose an HTTP endpoint on port 5000. 

## Run the client app

```bash
cd /git-clone-folder/client
code .
dotnet build
dotnet run
```

This will open a simple console app that will try to access the previously exposed endpoint.
Check the source code for more details

## Service Fabric

Big bonus points for whoever manages to run the HTTP api inside a Service Fabric platform. 

Installation details here:

1. https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-get-started

For Ubuntu users, that works only on Ubuntu 16.04 (Xenial). Don't even try using it on another releae, you'll just waste your time. 

